if !exists("g:syntax_on")
  syntax enable
endif
set t_Co=256

set backspace=indent,eol,start
set nu

colorscheme vim-material

